﻿using System;

namespace SecretSanta.Shared.Dtos
{
    public class Elf
    {
        public Guid Id { get; set; }
        public Player RealIdentity { get; set; }
        public string MatchWith { get; set; }
    }
}
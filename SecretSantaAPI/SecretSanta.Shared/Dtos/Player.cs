﻿using System;
using System.ComponentModel.DataAnnotations;

namespace SecretSanta.Shared.Dtos
{
    public class Player
    {
        public Guid Id { get; set; }

        [Required]
        [EmailAddress]
        public string Email { get; set; }

        [Required]
        public string Name { get; set; }

        public string PictureLink { get; set; }
    }
}
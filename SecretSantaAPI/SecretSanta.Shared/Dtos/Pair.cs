﻿using System;

namespace SecretSanta.Shared.Dtos
{
    public class Pair
    {
        public Guid Id { get; set; }
        public string Name { get; set; }
        public string MatchWith { get; set; }
    }
}
﻿using System;
using System.Collections.Generic;
using System.Security.Cryptography;

namespace SecretSanta.Shared.Helpers
{
    public static class RandomNumberHelper
    {
        public static int RandomIntFromRNG(int min, int max)
        {
            using (var rng = RandomNumberGenerator.Create())
            {
                // Generate 16 random bytes
                byte[] bytes = new byte[8];
                rng.GetNonZeroBytes(bytes);

                // Convert the bytes to a UInt32
                ulong scale = BitConverter.ToUInt64(bytes, 0);

                // And use that to pick a random number >= min and < max
                return (int)(min + (max - min) * (scale / (ulong.MaxValue + 1.0)));
            }
        }

        public static int RandomIntFromRNG(int rangeFromZero) => RandomIntFromRNG(0, rangeFromZero - 1);

        public static T RandomItemInList<T>(IList<T> items)
        {
            var randIndex = RandomIntFromRNG(items.Count);
            return items[randIndex];
        }

    }
}
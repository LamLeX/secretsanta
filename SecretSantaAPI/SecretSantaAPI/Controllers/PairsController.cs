﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;
using SecretSanta.Shared.Dtos;
using SecretSanta.Shared.Helpers;
using SecretSantaAPI.Data;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SecretSantaAPI.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class PairsController : ControllerBase
    {
        private readonly SecretSantaApiContext _context;
        private readonly ILogger _logger;

        public PairsController(SecretSantaApiContext context, ILogger<PairsController> logger)
        {
            _context = context;
            _logger = logger;
        }

        // GET: api/Pairs
        [HttpGet]
        public async Task<ActionResult<IEnumerable<Pair>>> GetPairs()
        {
            return await _context.Pairs.ToListAsync();
        }

        // GET: api/Pairs/takeone
        [HttpGet("takeone")]
        [Authorize("write:pairs")]
        public async Task<ActionResult<Pair>> TakeOne()
        {
            var deleteSucceeded = false;
            var attempt = 0;
            while (!deleteSucceeded && attempt < 5)
            {
                attempt++;
                try
                {
                    var indexes = await _context.Pairs.Select(p => p.Id).ToListAsync();
                    var randIndex = RandomNumberHelper.RandomItemInList(indexes);

                    var pair = await _context.Pairs.FindAsync(randIndex);

                    if (pair == null)
                    {
                        return NotFound();
                    }

                    try
                    {
                        //try to delete
                        _context.Pairs.Remove(pair);
                        deleteSucceeded = (await _context.SaveChangesAsync()) != 0;
                    }
                    catch (DbUpdateConcurrencyException e)
                    {
                        _logger.LogError(e, $"Some body tried to take the same pair {pair.Id}");
                        continue;
                    }

                    if (deleteSucceeded)
                    {
                        return pair;
                    }
                }
                catch (Exception e)
                {
                    _logger.LogError(e, "Error at TakeOne");
                }
            }

            return NotFound();
        }

        // GET: api/Pairs/5
        [HttpGet("{id}")]
        public async Task<ActionResult<Pair>> GetPair(Guid id)
        {
            var pair = await _context.Pairs.FindAsync(id);

            if (pair == null)
            {
                return NotFound();
            }

            return pair;
        }

        // PUT: api/Pairs/5
        [HttpPut("{id}")]
        [Authorize("write:pairs")]
        public async Task<IActionResult> PutPair(Guid id, Pair pair)
        {
            if (id != pair.Id)
            {
                return BadRequest();
            }

            _context.Entry(pair).State = EntityState.Modified;

            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!PairExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return NoContent();
        }

        // POST: api/Pairs
        [HttpPost]
        [Authorize("write:pairs")]
        public async Task<ActionResult<Pair>> PostPair(Pair pair)
        {
            _context.Pairs.Add(pair);
            await _context.SaveChangesAsync();

            return CreatedAtAction("GetPair", new { id = pair.Id }, pair);
        }

        // DELETE: api/Pairs/5
        [HttpDelete("{id}")]
        [Authorize("write:pairs")]
        public async Task<ActionResult<Pair>> DeletePair(Guid id)
        {
            var pair = await _context.Pairs.FindAsync(id);
            if (pair == null)
            {
                return NotFound();
            }

            _context.Pairs.Remove(pair);
            await _context.SaveChangesAsync();

            return pair;
        }

        private bool PairExists(Guid id)
        {
            return _context.Pairs.Any(e => e.Id == id);
        }
    }
}

﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using SecretSanta.Shared.Dtos;
using SecretSantaAPI.Data;
using SecretSantaAPI.Models;
using SecretSantaAPI.Services;
using System;
using System.Linq;
using System.Threading.Tasks;

namespace SecretSantaAPI.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class PlayController : ControllerBase
    {
        private readonly IServiceProvider _serviceProvider;
        private readonly SecretSantaApiContext _context;
        private readonly ILogger _logger;
        private readonly IEmailService _emailService;

        public PlayController(
            IServiceProvider serviceProvider,
            SecretSantaApiContext context,
            ILogger<PlayController> logger,
            IEmailService emailService)
        {
            _serviceProvider = serviceProvider;
            _context = context;
            _logger = logger;
            _emailService = emailService;
        }

        // POST: api/Play/setup
        [HttpPost("setup")]
        //[Authorize("write:setup")]
        public async Task<bool> Setup()
        {
            var elves = await _context.Elves.ToListAsync();

            foreach (var elf in elves)
            {
                var availablePairs = (await _context.Pairs.ToListAsync()).ToDictionary(p => p.Name, p => p.MatchWith);
                var tempMatch = elf.MatchWith;

                while (availablePairs.TryGetValue(tempMatch, out var newMatch))
                {
                    tempMatch = newMatch;
                }

                elf.MatchWith = tempMatch;
                await _context.SaveChangesAsync();
            }

            return true;
        }

        // POST: api/Play/register
        [HttpPost("register/{playerId?}")]
        [Authorize("write:register")]
        public async Task<ActionResult<string>> Register(Guid playerId)
        {
            var player = await _context.Players.FindAsync(playerId);

            if (player == null)
                return NotFound();

            var elf = await _context.Elves.FirstOrDefaultAsync(e => e.RealIdentity == player);

            if (elf == null)
            {
                var pair = (await _serviceProvider.GetService<PairsController>().TakeOne()).Value;

                if (pair == null)
                    return NotFound();

                elf = new Elf()
                {
                    RealIdentity = player,
                    MatchWith = pair.MatchWith
                };

                try
                {
                    await _context.AddAsync(elf);
                    await _context.SaveChangesAsync();
                }
                catch (DbUpdateConcurrencyException dbUpdateEx)
                {
                    _logger.LogError(dbUpdateEx, "Fail to add elf");
                    throw;
                }

                return pair.Name;
            }


            return BadRequest("Bad elf!!! Have you lost your secret identity already?");
        }


        [HttpGet("reInit")]
        public async Task<ActionResult<bool>> ReInit()
        {
            var firstQuery = await _context.Database
                .ExecuteSqlCommandAsync("DELETE FROM dbo.Elves");

            var secondQuery = await _context.Database
                .ExecuteSqlCommandAsync("DELETE FROM dbo.Pairs");

            DbInitializer.Initialize(_context);

            return true;
        }

        [HttpPost("sendInvite")]
        public async Task<ActionResult> SendEmail([FromBody] EmailCredential credential)
        {
            var players = await _context.Players.ToListAsync();

            foreach (var player in players)
            {
                var subject = $"Welcome {player.Name} to Secret Santa";
                var body = $@"Đây là invite cho {player.Name}: 
                            https://secretsantawebapp.azurewebsites.net/welcome/{player.Id}";
                await _emailService.SendEmail(player.Email, subject, body, credential);
            }

            return Ok(players.Select(p => (p.Name, p.Email)));
        }
    }
}

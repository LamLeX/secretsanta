﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using SecretSanta.Shared.Dtos;
using SecretSantaAPI.Data;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SecretSantaAPI.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class ElvesController : ControllerBase
    {
        private readonly SecretSantaApiContext _context;

        public ElvesController(SecretSantaApiContext context)
        {
            _context = context;
        }

        // GET: api/Elves
        [HttpGet]
        public async Task<ActionResult<IEnumerable<Elf>>> GetElves()
        {
            return await _context.Elves.Include(e => e.RealIdentity).ToListAsync();
        }

        // GET: api/Elves/5
        [HttpGet("{id}")]
        public async Task<ActionResult<Elf>> GetElf(Guid id)
        {
            var elf = await _context.Elves.Include(e => e.RealIdentity).FirstOrDefaultAsync(e => e.Id == id);

            if (elf == null)
            {
                return NotFound();
            }

            return elf;
        }

        // PUT: api/Elves/5
        [HttpPut("{id}")]
        [Authorize("write:elf")]
        public async Task<IActionResult> PutElf(Guid id, Elf elf)
        {
            if (id != elf.Id)
            {
                return BadRequest();
            }

            _context.Entry(elf).State = EntityState.Modified;

            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!ElfExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return NoContent();
        }

        // POST: api/Elves
        [HttpPost]
        [Authorize("write:elf")]
        public async Task<ActionResult<Elf>> PostElf(Elf elf)
        {
            _context.Elves.Add(elf);
            await _context.SaveChangesAsync();

            return CreatedAtAction("GetElf", new { id = elf.Id }, elf);
        }

        // DELETE: api/Elves/5
        [HttpDelete("{id}")]
        [Authorize("write:elf")]
        public async Task<ActionResult<Elf>> DeleteElf(Guid id)
        {
            var elf = await _context.Elves.FindAsync(id);
            if (elf == null)
            {
                return NotFound();
            }

            _context.Elves.Remove(elf);
            await _context.SaveChangesAsync();

            return elf;
        }

        private bool ElfExists(Guid id)
        {
            return _context.Elves.Any(e => e.Id == id);
        }
    }
}

﻿using Microsoft.Extensions.Configuration;
using SecretSantaAPI.Models;
using System.Net;
using System.Net.Mail;
using System.Threading.Tasks;

namespace SecretSantaAPI.Services
{
    public class EmailService : IEmailService
    {
        private readonly IConfiguration _configuration;
        public EmailService(IConfiguration configuration)
        {
            _configuration = configuration;
        }
        public async Task SendEmail(
            string toEmail,
            string subject,
            string message,
            EmailCredential fromEmail)
        {
            using (var client = new SmtpClient())
            {
                var credential = new NetworkCredential
                {
                    UserName = fromEmail.User,
                    Password = fromEmail.Password
                };

                client.Credentials = credential;
                client.Host = _configuration["Gmail:Host"];
                client.Port = int.Parse(_configuration["Gmail:Port"]);
                client.EnableSsl = true;

                using (var emailMessage = new MailMessage())
                {
                    emailMessage.To.Add(new MailAddress(toEmail));
                    emailMessage.From = new MailAddress(fromEmail.User);
                    emailMessage.Subject = subject;
                    emailMessage.Body = message;
                    client.Send(emailMessage);
                }
            }
            await Task.CompletedTask;
        }
    }
}
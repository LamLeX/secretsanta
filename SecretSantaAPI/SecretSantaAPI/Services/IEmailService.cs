﻿using SecretSantaAPI.Models;
using System.Threading.Tasks;

namespace SecretSantaAPI.Services
{
    public interface IEmailService
    {
        Task SendEmail(string toEmail, string subject, string message, EmailCredential fromEmail);
    }
}
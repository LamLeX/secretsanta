﻿namespace SecretSantaAPI.Models
{
    public class EmailCredential
    {
        public string User { get; set; }
        public string Password { get; set; }
    }
}
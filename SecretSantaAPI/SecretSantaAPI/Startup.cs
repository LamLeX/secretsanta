﻿using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using SecretSantaAPI.Authorization;
using SecretSantaAPI.Data;
using SecretSantaAPI.Services;
using Swashbuckle.AspNetCore.Swagger;

namespace SecretSantaAPI
{
    public class Startup
    {
        private readonly IHostingEnvironment _currentEnvironment;

        public Startup(IConfiguration configuration, IHostingEnvironment currentEnvironment)
        {
            Configuration = configuration;
            _currentEnvironment = currentEnvironment;
        }


        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            if (_currentEnvironment.IsDevelopment())
            {
                services.AddDbContext<SecretSantaApiContext>(options =>
                {
                    options.UseSqlServer(Configuration.GetConnectionString("SecretSantaAPI_dev"));
                });
            }
            else
            {
                services.AddDbContext<SecretSantaApiContext>(options =>
                {
                    options.UseSqlServer(Configuration.GetConnectionString("SecretSantaAPI_db_connection"));
                });
            }

            services.AddCors(options =>
            {
                options.AddPolicy("AllowSpecificOrigins",
                    builder =>
                    {
                        builder
                            .WithOrigins(
                                "https://secretsantawebapp.azurewebsites.net",
                                "http://localhost:4200")
                            .AllowAnyHeader()
                            .AllowCredentials();
                    });
            });

            services.AddMvc()
                .SetCompatibilityVersion(CompatibilityVersion.Version_2_1)
                .AddControllersAsServices();

            // 1. Add Authentication Services
            string domain = $"https://{Configuration["Auth0:Domain"]}/";
            services.AddAuthentication(options =>
            {
                options.DefaultAuthenticateScheme = JwtBearerDefaults.AuthenticationScheme;
                options.DefaultChallengeScheme = JwtBearerDefaults.AuthenticationScheme;

            }).AddJwtBearer(options =>
            {
                options.Authority = domain;
                options.Audience = Configuration["Auth0:ApiIdentifier"];
            });

            services.AddAuthorization(options =>
            {
                var policies = new[] { "write:elf", "write:pairs", "write:setup", "write:register", };
                foreach (var p in policies)
                {
                    options.AddPolicy(p,
                        policy => policy.Requirements.Add(new HasScopeRequirement(p, domain)));
                }
            });

            // register the scope authorization handler
            services.AddSingleton<IAuthorizationHandler, HasScopeHandler>();

            services.AddSwaggerGen(c =>
            {
                c.SwaggerDoc("v1", new Info { Title = "Secret Santa API", Version = "v1" });
            });

            services.AddSingleton<IEmailService, EmailService>();
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IHostingEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }
            else
            {
                app.UseHsts();
            }

            app.UseHttpsRedirection();

            app.UseCors("AllowSpecificOrigins");

            // 2. Enable authentication middleware
            app.UseAuthentication();

            app.UseMvc();

            app.UseSwagger(c =>
            {
                c.RouteTemplate = "api-docs/{documentName}/swagger.json";
            });

            app.UseSwaggerUI(c =>
            {
                c.SwaggerEndpoint("/api-docs/v1/swagger.json", "Secret Santa API V1");
                c.RoutePrefix = "api-docs";

                //OAuth
                //c.OAuthClientId("test-id");
                //c.OAuthClientSecret("test-secret");
                //c.OAuthRealm("test-realm");
                //c.OAuthAppName("test-app");
                //c.OAuthScopeSeparator(" ");
                //c.OAuthAdditionalQueryStringParams(new Dictionary<string, string> { { "foo", "bar" } });
                //c.OAuthUseBasicAuthenticationWithAccessCodeGrant();

            });
        }
    }
}

﻿using SecretSanta.Shared.Dtos;
using SecretSanta.Shared.Helpers;
using System;
using System.Collections.Generic;
using System.Linq;

namespace SecretSantaAPI.Data
{
    public static class DbInitializer
    {
        public static void Initialize(SecretSantaApiContext context)
        {
            context.Database.EnsureCreated();

            // Look for any students.
            if (context.Pairs.Any())
            {
                return;   // DB has been seeded
            }

            var elfNames = @"Rhino Pecs Glitterdrop
                            Notorious-P Jazynne
                            Angry Rock Tractorgreaser
                            SteelAngel Flamesnarl
                            Lucy-Sue Hatebite
                            Blackdrip Potatohauler
                            Sexy Biceps Catspoon
                            Fly - P Beetleflight
                            SamhainSorrow Cog
                            Stewcurse Darkspit
                            Clearnose Dazzlesprig 
                            Jellyshine Twinklepuff 
                            Twirlmoon Sparklerain 
                            Snowwind Startoes 
                            Honeydew Dazzleberry
                            Goldcloud Gentlespark 
                            Cloverflower Twirlflower 
                            Beetlesheen Dazzlespice 
                            Honeydew Twirlwind 
                            Jellyshy Sunbeam
                            Moonbell Twirleyes 
                            Jellysprig Snowglow 
                            Honeymint Sugartoes 
                            Sweetpuff Cloverfluff 
                            Oakstar Gentlefrost
                            Moonkiss Flutterdust 
                            Goldpuff Sugarshy 
                            Gentlesheen Sparklesprig 
                            Applefrost Twirlbud 
                            Willowspark Sparklegleam
                            Quickberry Applemint 
                            Gigglesprig Silverstar 
                            Jellyflower Glitterbeam 
                            Moontoes Twinklebreeze 
                            Oakcloud Sugargleam
                            Beetleshine Sugarstar 
                            Twinklespark Silverspice 
                            Goldmist Jellycloud 
                            Honeyrain Dazzledew 
                            Flittercloud Quickbud
                            Glittersprig Silverstar 
                            Flutterbud Twinkletoes 
                            Sugardrop Oakdance 
                            Willowtouch Cedarsprig 
                            Moondew Cleargaze
                            Sweetsheen Flutterflower 
                            Jellydew Silvershy 
                            Goldmint Gigglestar 
                            Stargleam Applesmile 
                            Willowfeather Moonberry".Split(Environment.NewLine, StringSplitOptions.RemoveEmptyEntries).Select(n => n.Trim());
            var elfNameShuffled = @"Jellydew Silvershy
                                  Beetlesheen Dazzlespice 
                                  Flittercloud Quickbud
                                  SamhainSorrow Cog
                                  Applefrost Twirlbud 
                                  Rhino Pecs Glitterdrop
                                  Jellyshine Twinklepuff 
                                  Moontoes Twinklebreeze 
                                  Honeydew Dazzleberry
                                  Blackdrip Potatohauler
                                  Willowtouch Cedarsprig 
                                  Quickberry Applemint 
                                  Angry Rock Tractorgreaser
                                  Honeymint Sugartoes 
                                  Willowspark Sparklegleam
                                  Goldmint Gigglestar 
                                  Twirlmoon Sparklerain 
                                  Gigglesprig Silverstar 
                                  Stewcurse Darkspit
                                  Sweetpuff Cloverfluff 
                                  Jellyflower Glitterbeam 
                                  Snowwind Startoes 
                                  Sexy Biceps Catspoon
                                  Cloverflower Twirlflower 
                                  Moonbell Twirleyes 
                                  Twinklespark Silverspice
                                  Lucy-Sue Hatebite
                                  Goldpuff Sugarshy 
                                  Sweetsheen Flutterflower 
                                  Stargleam Applesmile 
                                  Goldmist Jellycloud 
                                  Flutterbud Twinkletoes 
                                  Clearnose Dazzlesprig 
                                  Willowfeather Moonberry
                                  Notorious-P Jazynne
                                  Sugardrop Oakdance 
                                  Oakstar Gentlefrost
                                  Jellysprig Snowglow 
                                  Moonkiss Flutterdust 
                                  Beetleshine Sugarstar 
                                  Oakcloud Sugargleam
                                  Gentlesheen Sparklesprig 
                                  Moondew Cleargaze
                                  Honeyrain Dazzledew 
                                  Glittersprig Silverstar 
                                  Honeydew Twirlwind 
                                  Goldcloud Gentlespark 
                                  Fly - P Beetleflight
                                  SteelAngel Flamesnarl
                                  Jellyshy Sunbeam".Split(Environment.NewLine, StringSplitOptions.RemoveEmptyEntries).Select(n => n.Trim());

            //var pairs = elfNames.Zip(elfNameShuffled, (name, match) => new Pair() { Name = name, MatchWith = match });

            var pool = elfNames.ToList();
            var pairs = new List<Pair>();
            var firstMatch = RandomNumberHelper.RandomItemInList(pool);
            pool.Remove(firstMatch);
            var currentMatch = RandomNumberHelper.RandomItemInList(pool);
            pool.Remove(currentMatch);
            var firstPair = new Pair() { Name = firstMatch, MatchWith = currentMatch };
            pairs.Add(firstPair);

            while (pool.Any())
            {
                var newMatch = RandomNumberHelper.RandomItemInList(pool);
                pool.Remove(newMatch);

                var newPair = new Pair() { Name = currentMatch, MatchWith = newMatch };
                pairs.Add(newPair);

                currentMatch = newMatch;
            }
            var lastPair = new Pair() { Name = currentMatch, MatchWith = firstMatch };
            pairs.Add(lastPair);

            context.Pairs.AddRange(pairs);
            context.SaveChanges();
        }
    }

}
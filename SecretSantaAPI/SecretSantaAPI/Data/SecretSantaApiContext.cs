﻿using Microsoft.EntityFrameworkCore;
using SecretSanta.Shared.Dtos;

namespace SecretSantaAPI.Data
{
    public class SecretSantaApiContext : DbContext
    {
        public SecretSantaApiContext(DbContextOptions<SecretSantaApiContext> options) : base(options)
        {
        }

        public DbSet<Pair> Pairs { get; set; }
        public DbSet<Player> Players { get; set; }
        public DbSet<Elf> Elves { get; set; }
    }
}
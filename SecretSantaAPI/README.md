**SecretSanta.Shared**: The shared NET Standard 2 library for `SecretSantaAPI` and `SecretSantaPrototype`

**SecretSantaAPI**: the back-end api code

**SecretSantaPrototype**: the console code to test the data initializer algorithm
import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { PlayComponent } from './play/play.component';
import { WelcomeComponent } from './welcome/welcome.component';
import { InstructionComponent } from './instruction/instruction.component';
import { DemoComponent } from './slot-machine-button/demo/demo.component';

const routes: Routes = [
  { path: 'play', component: PlayComponent },
  { path: 'welcome/:id', component: WelcomeComponent },
  { path: 'instruction', component: InstructionComponent },
  {
    path: '',
    pathMatch: 'full',
    redirectTo: '/play'
  },
  { path: 'testDemo', component: DemoComponent }
];

@NgModule({
  imports: [
    RouterModule.forRoot(routes)
  ],
  exports: [RouterModule]
})
export class AppRoutingModule { }

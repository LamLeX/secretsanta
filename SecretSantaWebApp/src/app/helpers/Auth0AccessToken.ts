export interface Auth0AccessToken{
    access_token:string,
    token_type:string
}
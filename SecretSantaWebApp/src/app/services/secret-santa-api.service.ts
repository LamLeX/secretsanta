import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { filter, first, exhaustMap } from 'rxjs/operators';

import { Auth0AccessToken } from '../helpers/Auth0AccessToken';
import { Player } from '../models/player';
import { Observable } from 'rxjs';


@Injectable({
  providedIn: 'root'
})
export class SecretSantaApiService {

  private authUrl = 'https://secretsanta.auth0.com/oauth/token';
  private authBody =
    {
      client_id: 'oWHv825F4EvFRJANv2idZwTfE5afO6UU',
      client_secret: 'YSk-eMlHg0Pn8UW4BBiXe3Ms3YtbimYfIRIf_8TvHuAU3Bs_aL2Pi_4iGcPQOhTU',
      audience: 'https://secretsantaapi.azurewebsites.net/api',
      grant_type: 'client_credentials',
      scope: 'write:register,write:setup'
    };
  // private baseApiUrl = 'https://secretsantaapi.azurewebsites.net/';
  private baseApiUrl = 'https://localhost:5001/';

  constructor(private http: HttpClient) { }

  getAccessToken(): Observable<Auth0AccessToken> {
    return this.http.post<Auth0AccessToken>(this.authUrl, this.authBody,
      { headers: { 'content-type': 'application/json' } });
  }

  getSecretId(playerGuid: string): Observable<string> {
    return this.getAccessToken().pipe(
      filter(token => token != null),
      first(),
      exhaustMap(token => {
        const requestUrl = this.baseApiUrl + 'api/Play/register/' + playerGuid;
        const authString = token.token_type + ' ' + token.access_token;
        return this.http.post(requestUrl, undefined, { headers: { authorization: authString }, responseType: 'text' });
      }));
  }

  getPlayerInfo(playerGuid: string) {
    const requestUrl = this.baseApiUrl + 'api/Players/' + playerGuid;
    return this.http.get<Player>(requestUrl);
  }

  playSecretSanta(): Observable<boolean> {
    return this.getAccessToken().pipe(
      filter(token => token != null),
      first(),
      exhaustMap(token => {
        const requestUrl = this.baseApiUrl + 'api/Play/setup';
        const authString = token.token_type + ' ' + token.access_token;
        return this.http.post<boolean>(requestUrl, undefined, { headers: { authorization: authString } });
      }));
  }
}

import { TestBed } from '@angular/core/testing';

import { SecretSantaApiService } from './secret-santa-api.service';

describe('SecretSantaApiService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: SecretSantaApiService = TestBed.get(SecretSantaApiService);
    expect(service).toBeTruthy();
  });
});

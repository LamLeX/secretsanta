import { Component, OnInit, Inject } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';

import { SecretSantaApiService } from '../services/secret-santa-api.service';
import { tap } from 'rxjs/operators';
import { MatSnackBar } from '@angular/material/snack-bar';

export interface DialogData {
  guid: string;
  name: string;
}

@Component({
  selector: 'app-confirm-dialog',
  templateUrl: './confirm-dialog.component.html',
  styleUrls: ['./confirm-dialog.component.scss']
})
export class ConfirmDialogComponent implements OnInit {
  step = 0;
  public santaName: string = null;
  public nameAcquiring = false;
  public errorInRequest: string = null;

  constructor(
    public dialogRef: MatDialogRef<ConfirmDialogComponent>,
    @Inject(MAT_DIALOG_DATA) public data: DialogData,
    private santaService: SecretSantaApiService,
    private snackBar: MatSnackBar) { }

  onNoClick(): void {
    this.dialogRef.close(this.santaName);
  }

  ngOnInit() {
  }

  setStep(index: number) {
    this.step = index;
  }

  nextStep() {
    this.step++;
  }

  prevStep() {
    this.step--;
  }

  getSecretId() {
    if (this.santaName == null) {
      this.nameAcquiring = true;
      this.santaService.getSecretId(this.data.guid)
        .pipe(
          tap(undefined,
            error => this.errorInRequest = error.error))
        .subscribe(name => {
          this.santaName = name;
        });
    }
  }

  openSnackBar() {
    this.snackBar.open(
      '📋 Your secret santa name is copied to clipboard 📋',
      undefined,
      {
        duration: 5000,
      });
  }
}

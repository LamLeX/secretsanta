import { BrowserModule } from '@angular/platform-browser';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { NgModule } from '@angular/core';
import { HttpClientModule } from '@angular/common/http';

import { MaterialImportModule } from './material-import/material-import.module';
import { AppRoutingModule } from './app-routing.module';

import { ClipboardModule, ClipboardService } from 'ngx-clipboard';

import { AppComponent } from './app.component';
import { WelcomeComponent } from './welcome/welcome.component';
import { InstructionComponent } from './instruction/instruction.component';
import { PlayComponent } from './play/play.component';
import { ConfirmDialogComponent } from './confirm-dialog/confirm-dialog.component';
import { SecretSantaApiService } from './services/secret-santa-api.service';
import { ElvesService } from './services/elves.service';
import { SlotMachineButtonModule } from './slot-machine-button/slot-machine-button.module';
import { DemoComponent } from './slot-machine-button/demo/demo.component';

@NgModule({
  declarations: [
    AppComponent,
    WelcomeComponent,
    InstructionComponent,
    PlayComponent,
    ConfirmDialogComponent,
    DemoComponent
  ],
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    AppRoutingModule,
    HttpClientModule,
    MaterialImportModule,
    ClipboardModule,
    SlotMachineButtonModule
  ],
  providers: [SecretSantaApiService, ClipboardService, ElvesService],
  bootstrap: [AppComponent],
  entryComponents: [ConfirmDialogComponent]
})
export class AppModule { }

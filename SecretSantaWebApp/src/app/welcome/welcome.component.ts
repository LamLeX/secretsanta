import { Component, OnInit } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { ActivatedRoute } from '@angular/router';
import { Observable } from 'rxjs';

import { ConfirmDialogComponent } from '../confirm-dialog/confirm-dialog.component';
import { SecretSantaApiService } from '../services/secret-santa-api.service';
import { Player } from '../models/player';
import { first } from 'rxjs/operators';
import { ClipboardService } from 'ngx-clipboard';


@Component({
  selector: 'app-welcome',
  templateUrl: './welcome.component.html',
  styleUrls: ['./welcome.component.scss']
})
export class WelcomeComponent implements OnInit {

  public playerObservable: Observable<Player>;
  public player: Player;

  constructor(
    public dialog: MatDialog,
    private apiService: SecretSantaApiService,
    private route: ActivatedRoute,
    private _clipboardService: ClipboardService) { }

  ngOnInit() {
    this.playerObservable = this.getPlayer();
    this.playerObservable
      .pipe(first(p => p != null))
      .subscribe(p => this.player = p);
  }

  confirm() {

  }

  getPlayer() {
    const id = this.route.snapshot.paramMap.get('id');
    return this.apiService.getPlayerInfo(id);
  }

  openDialog(): void {
    const dialogRef = this.dialog.open(ConfirmDialogComponent,
      {
        width: '90%',
        height: '90%',
        data: { name: this.player.name, guid: this.player.id }
      });

    dialogRef.afterClosed().subscribe(result => {
      console.log('The dialog was closed');
      console.log(result);
      console.log(dialogRef.componentInstance.santaName);
      console.log(this._clipboardService.copyFromContent(dialogRef.componentInstance.santaName));
    });
  }
}
